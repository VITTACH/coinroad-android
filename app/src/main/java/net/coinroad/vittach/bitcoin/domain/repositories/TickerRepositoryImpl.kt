package net.coinroad.vittach.bitcoin.domain.repositories

import io.reactivex.Observable
import io.reactivex.Single
import net.coinroad.vittach.bitcoin.data.entity.toTickerModel
import net.coinroad.vittach.bitcoin.data.network.cmc.CMCApiService
import net.coinroad.vittach.bitcoin.domain.Failure
import net.coinroad.vittach.bitcoin.domain.Loading
import net.coinroad.vittach.bitcoin.domain.Resource
import net.coinroad.vittach.bitcoin.domain.Success
import net.coinroad.vittach.bitcoin.domain.models.*

interface TickerRepository {
    fun getTickers(currency: String, refresh: Boolean): Observable<Resource<List<TickerModel>>>
}

class TickerRepositoryImpl(
    private val cmcApiService: CMCApiService
) : TickerRepository {

    private val cache = mutableListOf<TickerModel>()

    override fun getTickers(
        currency: String,
        refresh: Boolean
    ): Observable<Resource<List<TickerModel>>> {
        val tickersSingle = if (refresh || cache.isEmpty()) {
            cmcApiService.getTickers(currency)
                .map { baseResponse ->
                    baseResponse.data?.mapNotNull { it.toTickerModel(currency) } ?: emptyList()
                }
                .doOnSuccess {
                    cache.clear()
                    cache.addAll(it)
                }
        } else {
            Single.just(cache)
        }

        return tickersSingle
            .map<Resource<List<TickerModel>>> { Success(it) }
            .onErrorReturn { Failure(it) }
            .toObservable()
            .startWith(Loading())

    }
}