package net.coinroad.vittach.bitcoin.widget

import android.view.View
import android.widget.RemoteViews
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.data.entity.TickerData
import net.coinroad.vittach.bitcoin.util.DecimalUtils
import java.text.SimpleDateFormat
import java.util.*

class WidgetProviderLarge : WidgetProvider() {
    companion object {
        fun bindView(ticker: TickerData, view: RemoteViews, precision: Int, currency: String): RemoteViews {
            val priceUsd = ticker.quote?.detail?.get(currency)?.priceUsd
            return view.apply {
                setTextViewText(R.id.exchange, ticker.id)
                setTextViewText(R.id.pair, ticker.symbol)
                setTextViewText(R.id.price, DecimalUtils.formatPrice(priceUsd, precision))
                setTextViewText(R.id.time, SimpleDateFormat("HH:mm:ss").format(Date()))
                setViewVisibility(R.id.open_high_low_text, View.VISIBLE)
                setViewVisibility(R.id.bid_ask_text, View.VISIBLE)
            }
        }
    }
}

class WidgetProviderMedium : WidgetProvider() {
    companion object {
        fun bindView(ticker: TickerData, view: RemoteViews, precision: Int, currency: String): RemoteViews {
            val priceUsd = ticker.quote?.detail?.get(currency)?.priceUsd
            return view.apply {
                setTextViewText(R.id.exchange, ticker.id)
                setTextViewText(R.id.pair, ticker.symbol)
                setTextViewText(R.id.price, DecimalUtils.formatPrice(priceUsd, precision))
                setTextViewText(R.id.time, SimpleDateFormat("HH:mm:ss").format(Date()))
                setViewVisibility(R.id.open_high_low_text, View.VISIBLE)
            }
        }
    }
}

class WidgetProviderSmall : WidgetProvider() {
    companion object {
        fun bindView(ticker: TickerData, view: RemoteViews, precision: Int, currency: String): RemoteViews {
            val priceUsd = ticker.quote?.detail?.get(currency)?.priceUsd
            return view.apply {
                setTextViewText(R.id.exchange, ticker.id)
                setTextViewText(R.id.pair, ticker.symbol)
                setTextViewText(R.id.price, DecimalUtils.formatPrice(priceUsd, precision))
                setTextViewText(R.id.time, SimpleDateFormat("HH:mm:ss").format(Date()))
            }
        }
    }
}