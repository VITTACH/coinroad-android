package net.coinroad.vittach.bitcoin.ui.news

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.Fragment
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_news.*
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.BaseActivity.Companion.primaryFragmentId


class NewsFragment : Fragment() {

    // https://medium.com/fungjai/playing-video-by-exoplayer-b97903be0b33

    companion object {
        private const val POS_KEY = "EXO_PLAYER_POS_KEY"
        private const val VIDEO_URL =
            "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_640_3MG.mp4"
    }

    private lateinit var params: ViewGroup.MarginLayoutParams
    private var initBottomMargin = 0

    private val simpleExoPlayer by lazy {
        ExoPlayerFactory.newSimpleInstance(context, DefaultTrackSelector(), DefaultLoadControl())
    }

    override fun onCreateView(inflater: LayoutInflater, group: ViewGroup?, bundle: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news, group, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        params = requireActivity().nav_host_container.layoutParams as ViewGroup.MarginLayoutParams
        initBottomMargin = params.bottomMargin

        bottom_nav.selectedItemId = R.id.news_graph

        playerView.player = simpleExoPlayer
        simpleExoPlayer.prepare(createMediaSource(VIDEO_URL))
        savedInstanceState?.getLong(POS_KEY)?.let {
            simpleExoPlayer.seekTo(it)
        }

        motion_container.setTransitionListener(
            object : MotionLayout.TransitionListener {
                override fun onTransitionTrigger(p0: MotionLayout?, r: Int, s: Boolean, p3: Float) {
                }

                override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
                }

                override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
                    setNavBarVisibility(p3 == 1f)
                }

                override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                }
            }
        )
    }

    override fun onPause() {
        super.onPause()
        simpleExoPlayer.playWhenReady = false
    }

    override fun onResume() {
        super.onResume()
        simpleExoPlayer.playWhenReady = true;
    }

    override fun onStart() {
        super.onStart()

        params.bottomMargin = 0
        primaryFragmentId = R.id.news_graph
        setNavBarVisibility(false)
    }

    override fun onStop() {
        super.onStop()

        params.bottomMargin = initBottomMargin
        simpleExoPlayer.release()
        setNavBarVisibility(true)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putLong(POS_KEY, simpleExoPlayer.currentPosition)
        super.onSaveInstanceState(outState)
    }

    private fun setNavBarVisibility(visible: Boolean) {

        if (visible) {
            bottom_nav.visibility = View.INVISIBLE
            activity?.bottomNav?.visibility = View.VISIBLE
        } else {
            bottom_nav.visibility = View.VISIBLE
            activity?.bottomNav?.visibility = View.GONE
        }
    }

    private fun createMediaSource(videoUrl: String): MediaSource {
        val userAgent = Util.getUserAgent(context, getString(R.string.app_name))

        return ExtractorMediaSource(
            Uri.parse(videoUrl),
            DefaultHttpDataSourceFactory(userAgent),
            DefaultExtractorsFactory(),
            null,
            null
        )
    }
}
