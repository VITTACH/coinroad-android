package net.coinroad.vittach.bitcoin.ui.opnedcoin

import net.coinroad.vittach.bitcoin.BaseActivity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import net.coinroad.vittach.R.layout.activity_opened_coin
import net.coinroad.vittach.bitcoin.domain.models.TickerModel
import net.coinroad.vittach.databinding.ActivityOpenedCoinBinding


class OpenedCoinActivity : BaseActivity() {

    companion object {
        private const val SELECTED_TICKER = "selected_ticker"

        fun createIntent(context: Context?, tickerModel: TickerModel): Intent {
            return Intent(context, OpenedCoinActivity::class.java).putExtra(SELECTED_TICKER, tickerModel)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bind: ActivityOpenedCoinBinding = DataBindingUtil.setContentView(this, activity_opened_coin)

        intent.extras?.let {
            bind.ticker = it.getSerializable(SELECTED_TICKER) as TickerModel
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}