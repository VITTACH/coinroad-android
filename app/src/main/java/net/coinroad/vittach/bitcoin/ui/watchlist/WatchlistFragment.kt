package net.coinroad.vittach.bitcoin.ui.watchlist

import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.hardware.SensorManager.AXIS_X
import android.hardware.SensorManager.AXIS_Z
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_watchlist.*
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.BaseActivity
import net.coinroad.vittach.bitcoin.ui.watchlist.RecyclerWatchlistAdapter.Companion.ITEM_ROWS
import net.coinroad.vittach.bitcoin.ui.watchlist.RecyclerWatchlistAdapter.Companion.VIEW_TYPE_ADS
import net.coinroad.vittach.bitcoin.ui.watchlist.RecyclerWatchlistAdapter.Companion.VIEW_TYPE_TICKER
import net.coinroad.vittach.bitcoin.domain.Failure
import net.coinroad.vittach.bitcoin.domain.Loading
import net.coinroad.vittach.bitcoin.domain.Success
import net.coinroad.vittach.bitcoin.domain.models.TickerModel
import net.coinroad.vittach.bitcoin.ui.opnedcoin.OpenedCoinActivity
import net.coinroad.vittach.bitcoin.viewmodel.TickerViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import java.lang.Integer.max
import java.lang.Integer.min
import kotlin.math.absoluteValue


class WatchlistFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var gridLayout: GridLayoutManager
    private lateinit var watchlistAdapter: RecyclerWatchlistAdapter

    private val tickerVM: TickerViewModel by viewModel()

    private var snackbar: Snackbar? = null

    private var offsetY = 0

    private var orientation = Configuration.ORIENTATION_PORTRAIT

    private lateinit var provider: String

    private var sensorManager: SensorManager? = null
    private var sensorListener: SensorEventListener? = null
    private var sensor: Sensor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        provider = arguments?.getString(PROVIDER) ?: throw IllegalArgumentException("Provider need")

        // CoinRoadApplication.appComponent.inject(this)

        sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as? SensorManager
        sensor = sensorManager?.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
        sensorListener = object : SensorEventListener {
            var oldOrientation = orientation
            val toolBarHeight: Int = 0
                get() {
                    return if (field == 0) {
                        (activity as BaseActivity).getToolBarHeight()
                    } else field
                }

            override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
            }

            override fun onSensorChanged(sensorEvent: SensorEvent) {
                val rotations = FloatArray(3)
                val rotateMatrix = FloatArray(16)
                val newRotationMatrix = FloatArray(16)

                // Convert to orientations
                SensorManager.getRotationMatrixFromVector(rotateMatrix, sensorEvent.values)
                SensorManager.remapCoordinateSystem(rotateMatrix, AXIS_X, AXIS_Z, newRotationMatrix)
                SensorManager.getOrientation(newRotationMatrix, rotations)

                for (i in 0..2) {
                    rotations[i] = Math.toDegrees(rotations[i].toDouble()).toFloat()
                }
                if (rotations[0] > 0) {
                    return
                }
                val rotationZ = -rotations[2]

                swipeRefresh.pivotY = baseView.height / 2f - toolBarHeight.toFloat()
                swipeRefresh.pivotX = baseView.width / 2f
                swipeRefresh.rotation = rotationZ.toStickyDegrees()

                orientation = rotationZ.toOrientation()

                if (orientation != oldOrientation) {
                    updateOrientation()
                    recyclerView.invalidate()
                    setStickyOrientation(rotationZ, toolBarHeight)
                    oldOrientation = orientation
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

        if (userVisibleHint) {
            tickerVM.loadTickers()
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        sensorManager?.registerListener(sensorListener, sensor, SensorManager.SENSOR_DELAY_FASTEST)
    }

    override fun onPause() {
        super.onPause()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
        sensorManager?.unregisterListener(sensorListener)
    }

    override fun onCreateView(inflater: LayoutInflater, group: ViewGroup?, bundle: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_watchlist, group, false)

        PreferenceManager.setDefaultValues(context, R.xml.preference_general, false)
        PreferenceManager.setDefaultValues(context, R.xml.preference_notification, false)

        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }

        watchlistAdapter = RecyclerWatchlistAdapter()
            .apply {
                setClickListener(object : RecyclerWatchlistAdapter.ItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        startActivity(
                            OpenedCoinActivity.createIntent(context, this@apply.getItem(position))
                        )
                    }
                })
            }

        updateOrientation()

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            private var isHeaderInit = false
            private var downY = 0
            private var upY = 0

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val adsHeight = watchlistAdapter.getRowHeight(VIEW_TYPE_ADS)
                val tickerHeight = watchlistAdapter.getRowHeight(VIEW_TYPE_TICKER)
                val blockHeight = adsHeight + ITEM_ROWS * tickerHeight

                offsetY += dy

                val adsCount = offsetY / blockHeight + min(offsetY / adsHeight, 1)
                val tickersHeight = offsetY - adsCount * adsHeight
                val tickersCount = tickersHeight / tickerHeight
                val rowCount = adsCount + tickersCount

                with(stickHeaderView) {
                    if ((rowCount + 1) % (ITEM_ROWS + 1) == 0) {
                        if (!isHeaderInit) {
                            isHeaderInit = true
                            translationY = (if (dy >= 0) upY else downY).toFloat()
                        }
                        translationY -= dy
                    } else if (isHeaderInit) {
                        translationY = 0f
                        isHeaderInit = false
                    }
                }

                downY = min(-tickerHeight, tickersCount * tickerHeight - adsHeight - tickersHeight)
                upY = (tickersCount + 1) * tickerHeight - tickersHeight

                (activity as BaseActivity).updateRowIndex(rowCount)
            }
        })

        recyclerView.apply { adapter = watchlistAdapter }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tickerVM.tickerLiveData.observe(viewLifecycleOwner, Observer { resource ->
            if (!userVisibleHint) return@Observer

            when (resource) {
                is Success<List<TickerModel>> -> {
                    resource.data.let { tickers ->
                        watchlistAdapter.setTickers(ArrayList(tickers))
                        stickHeaderView.visibility = View.VISIBLE
                    }
                    snackbar?.dismiss()
                    swipeRefresh.isRefreshing = false
                }

                is Failure<List<TickerModel>> -> {
                    stickHeaderView.visibility = View.GONE
                    showSnackbar(getString(R.string.network_error, resource.throwable))
                    swipeRefresh.isRefreshing = false
                }

                is Loading -> {
                    swipeRefresh.isRefreshing = true
                }
            }
        })
    }

    override fun onConfigurationChanged(configurations: Configuration) {
        super.onConfigurationChanged(configurations)
        orientation = configurations.orientation
        gridLayout.spanCount = getLayoutSpan()
    }

    override fun onViewCreated(view: View, instanceState: Bundle?) {
        super.onViewCreated(view, instanceState)

        swipeRefresh.setOnRefreshListener { tickerVM.loadTickers(true) }
        swipeRefresh.setColorSchemeColors(Color.GRAY)
    }

    private fun showSnackbar(title: String) {
        snackbar = Snackbar.make(requireView(), title, Snackbar.LENGTH_INDEFINITE)
            .apply {
                (view.findViewById<View>(R.id.snackbar_text) as TextView).apply {
                    maxLines = 4
                }
                setAction(R.string.network_action) {
                    tickerVM.loadTickers(true)
                    dismiss()
                }
            }

        snackbar?.show()
    }

    private fun getLayoutSpan(): Int {
        return if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            SPAN_COUNT_LANDSCAPE
        } else SPAN_COUNT_PORTRAIT
    }

    private fun changeLayoutManager(context: Context) {
        gridLayout = GridLayoutManager(context, getLayoutSpan())
        gridLayout.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (watchlistAdapter.getItemViewType(position)) {
                    VIEW_TYPE_ADS -> max(gridLayout.spanCount / 2, SPAN_COUNT_PORTRAIT)
                    VIEW_TYPE_TICKER -> 1
                    else -> 1
                }
            }
        }

        recyclerView.layoutManager = gridLayout
    }

    private fun updateOrientation() {
        changeLayoutManager(requireContext())
        watchlistAdapter.setRowSize(gridLayout.spanCount)
        watchlistAdapter.setLayoutOrientation(orientation)
    }

    private fun Float.toOrientation(): Int {
        return if (this.absoluteValue in 70.0f..110.0f) {
            Configuration.ORIENTATION_LANDSCAPE
        } else {
            Configuration.ORIENTATION_PORTRAIT
        }
    }

    private fun Float.toStickyDegrees(): Float {
        return if (this in -20.0f..20.0f) {
            0f
        } else if (this > 160.0f || this < -160.0f) {
            180f
        } else if (this in 70.0f..110.0f) {
            90f
        } else if (this in -110.0f..-70.0f) {
            -90f
        } else {
            this
        }
    }

    private fun setStickyOrientation(rotationDegree: Float, toolBarHeight: Int) {
        val initWidth = baseView.width
        val initHeight = baseView.height
        with(swipeRefresh) {
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                val newWidth = initHeight - toolBarHeight
                layoutParams = LayoutParams(newWidth, initWidth)
                if (rotationDegree < 0) {
                    translationX = (initHeight - initWidth) / 2f - toolBarHeight
                    translationY = (newWidth - initWidth + toolBarHeight) / 2f
                } else {
                    translationX = (initWidth - initHeight) / 2f + toolBarHeight
                    translationY = -(newWidth - initWidth - toolBarHeight) / 2f
                }
            } else {
                layoutParams = LayoutParams(initWidth, initHeight)
                translationY = 0f
                translationX = 0f
            }
        }
    }

    companion object {
        const val SPAN_COUNT_LANDSCAPE = 6
        const val SPAN_COUNT_PORTRAIT = 3

        const val PROVIDER = "coin_watchlist_provider"

        fun newInstance(provider: String): WatchlistFragment {
            return WatchlistFragment().apply {
                arguments = Bundle().apply {
                    putString(PROVIDER, provider)
                }
            }
        }
    }
}
