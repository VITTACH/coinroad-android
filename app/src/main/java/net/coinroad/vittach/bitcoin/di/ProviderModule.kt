package net.coinroad.vittach.bitcoin.di

import android.content.Context
import dagger.Module
import dagger.Provides
import net.coinroad.vittach.bitcoin.data.network.cmc.CMCApiService
import net.coinroad.vittach.bitcoin.data.storage.TotpPreferences
import net.coinroad.vittach.bitcoin.data.storage.TotpPreferencesImpl
import net.coinroad.vittach.bitcoin.domain.repositories.TickerRepository
import net.coinroad.vittach.bitcoin.domain.repositories.TickerRepositoryImpl
import net.coinroad.vittach.bitcoin.domain.repositories.TotpRepository
import net.coinroad.vittach.bitcoin.domain.repositories.TotpRepositoryImpl
import net.coinroad.vittach.bitcoin.viewmodel.TickerViewModel
import net.coinroad.vittach.bitcoin.viewmodel.TotpViewModel

@Module
class ProviderModule {

    @Provides
    fun providerTickerRepository(apiService: CMCApiService): TickerRepository {
        return TickerRepositoryImpl(apiService)
    }

    @Provides
    fun provideTickerViewModel(repository: TickerRepository): TickerViewModel {
        return TickerViewModel(repository)
    }

    @Provides
    fun provideTotpPreferences(context: Context): TotpPreferences {
        return TotpPreferencesImpl(context)
    }

    @Provides
    fun providerTotpRepository(preferences: TotpPreferences): TotpRepository {
        return TotpRepositoryImpl(preferences)
    }

    @Provides
    fun provideTotpViewModel(repository: TotpRepository): TotpViewModel {
        return TotpViewModel(repository)
    }
}