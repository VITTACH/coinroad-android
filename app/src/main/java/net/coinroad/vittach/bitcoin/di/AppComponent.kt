package net.coinroad.vittach.bitcoin.di

import dagger.Component
import net.coinroad.vittach.bitcoin.ui.watchlist.WatchlistFragment
import javax.inject.Singleton

@Component(modules = [
    AppModule::class,
    ProviderModule::class
])
@Singleton
interface AppComponent {

    fun inject(fragment: WatchlistFragment)
}