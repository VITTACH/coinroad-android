package net.coinroad.vittach.bitcoin.data.network.cmc

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import net.coinroad.vittach.BuildConfig
import net.coinroad.vittach.bitcoin.data.entity.BaseResponse
import net.coinroad.vittach.bitcoin.data.entity.TickerData
import net.coinroad.vittach.bitcoin.data.network.ApiFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

class CMCApiService(private val apiFactory: ApiFactory) {

    private val service by lazy { createCmcApiService() }

    private fun createCmcApiService(): CMCApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(JacksonConverterFactory.create(apiFactory.createObjectMapper()))
            .client(apiFactory.okHttpClient)
            .build()
            .create(CMCApi::class.java)
    }

    fun getTickers(currency: String): Single<BaseResponse<TickerData>> {
        return service.getTickers(currency, BuildConfig.CMC_API_KEY)
    }

    fun getTicker(tickerId: String): Single<BaseResponse<TickerData>> {
        return service.getTicker(tickerId, BuildConfig.CMC_API_KEY)
    }

    companion object {
        private const val BASE_URL = "https://pro-api.coinmarketcap.com"
    }
}