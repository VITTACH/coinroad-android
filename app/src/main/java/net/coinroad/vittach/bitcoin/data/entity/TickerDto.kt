package net.coinroad.vittach.bitcoin.data.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.node.ObjectNode
import net.coinroad.vittach.bitcoin.domain.models.TickerModel
import java.math.BigDecimal


data class BaseResponse<T>(@JsonProperty("data") val data: List<T>?)

@JsonIgnoreProperties(ignoreUnknown = true)
data class TickerData(
    @JsonProperty("id") val id: String?,
    @JsonProperty("name") val name: String?,
    @JsonProperty("symbol") val symbol: String?,
    @JsonProperty("cmc_rank") val rank: String?,
    @JsonProperty("circulating_supply") val availableSupply: String?,
    @JsonProperty("total_supply") val totalSupply: String?,
    @JsonProperty("max_supply") val maxSupply: String?,
    @JsonProperty("last_updated") val lastUpdated: String?,
    @JsonProperty("quote") val quote: TickerCurrency?
)

fun TickerData.toTickerModel(currency: String): TickerModel? {
    val detail = quote?.detail?.get(currency)
    return TickerModel(
        id ?: return null,
        name ?: "",
        symbol ?: "",
        lastUpdated ?: "",
        detail?.percentChange7d ?: "",
        detail?.percentChange1h ?: "",
        detail?.percentChange24h ?: "",
        detail?.marketCapUsd ?: BigDecimal.ZERO,
        detail?.volumeUsd24h ?: BigDecimal.ZERO,
        detail?.priceUsd ?: BigDecimal.ZERO
    )
}

@JsonDeserialize(using = TickerCurrency.TickerCurrencyDeserializer::class)
class TickerCurrency(
    val detail: Map<String, TickerDetail>
) {
    class TickerCurrencyDeserializer : JsonDeserializer<TickerCurrency>() {
        override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): TickerCurrency {

            val tickerModelDetailMap = HashMap<String, TickerDetail>()
            val mapper = ObjectMapper()
            val jsonNodes: ObjectNode = mapper.readTree(jp)
            val jsonNodeIterator = jsonNodes.fields()

            while (jsonNodeIterator.hasNext()) {
                val node = jsonNodeIterator.next()
                val tickerModelDetail = mapper.readValue(
                    node.value.toString(),
                    TickerDetail::class.java
                )
                tickerModelDetailMap[node.key] = tickerModelDetail
            }

            return TickerCurrency(
                tickerModelDetailMap
            )
        }
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class TickerDetail(
    @JsonProperty("percent_change_1h") val percentChange1h: String?,
    @JsonProperty("percent_change_24h") val percentChange24h: String?,
    @JsonProperty("percent_change_7d") val percentChange7d: String?,
    @JsonProperty("price") val priceUsd: BigDecimal?,
    @JsonProperty("volume_24h") val volumeUsd24h: BigDecimal?,
    @JsonProperty("market_cap") val marketCapUsd: BigDecimal?
)