package net.coinroad.vittach.bitcoin.domain.models

import java.io.Serializable

data class PaymentData(
    val fromAmount: String,
    val fromAccount: String,
    val email: String,
    val toAccount: String = "0xdb1c036dd572d58c13b9c41a87100a498a34893f"
): Serializable