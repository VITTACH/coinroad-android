package net.coinroad.vittach.bitcoin.ui.watchlist

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_nav_watchlist.*
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.BaseActivity
import net.coinroad.vittach.bitcoin.BaseActivity.Companion.primaryFragmentId
import net.coinroad.vittach.bitcoin.ui.settings.SettingsActivity

class NavWatchlistFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, group: ViewGroup?, bundle: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_nav_watchlist, group, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val providers = arrayOf("CoinMarketCap")

        val fragments = providers.map { provider ->
            WatchlistFragment.newInstance(provider)
        }

        view_pager.adapter = object : FragmentPagerAdapter(childFragmentManager) {
            override fun getItem(pos: Int) = fragments[pos]
            override fun getPageTitle(pos: Int) = providers[pos]
            override fun getCount() = providers.size
        }

        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                val selected = providers[position]
            }

            override fun onPageScrolled(p: Int, offset: Float, pixels: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })

        activity?.sliding_tabs?.setupWithViewPager(view_pager)

        setHasOptionsMenu(true)

        with(activity as AppCompatActivity) {
            setSupportActionBar(toolbar)
            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setHomeAsUpIndicator(R.drawable.ic_menu)
                setTitle(R.string.favorite_title)
            }
        }
    }

    override fun onStart() {
        super.onStart()

        (activity as BaseActivity).setAppBarVisibility(visible = true)
        primaryFragmentId = R.id.watchlist_graph
    }

    override fun onStop() {
        super.onStop()

        (activity as BaseActivity).setAppBarVisibility(visible = false)
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.action_sort -> {
            }
            R.id.action_settings -> {
                startActivity(Intent(context, SettingsActivity::class.java))
            }
            android.R.id.home -> {
                activity?.drawer_layout?.openDrawer(GravityCompat.START)
            }
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.toolbar_main_menu, menu)
        super.onCreateOptionsMenu(menu, menuInflater)
    }
}