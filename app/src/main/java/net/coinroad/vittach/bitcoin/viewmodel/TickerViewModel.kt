package net.coinroad.vittach.bitcoin.viewmodel

import androidx.lifecycle.MutableLiveData
import net.coinroad.vittach.bitcoin.domain.Resource
import net.coinroad.vittach.bitcoin.domain.models.TickerModel
import net.coinroad.vittach.bitcoin.domain.repositories.TickerRepository
import net.coinroad.vittach.bitcoin.widget.WidgetProvider.Companion.WIDGET_CURRENCY


class TickerViewModel(
    private val repository: TickerRepository
) : BaseViewModel() {

    val tickerLiveData = MutableLiveData<Resource<List<TickerModel>>>()

    fun loadTickers(refresh: Boolean = false) {
        launch {
            repository.getTickers(WIDGET_CURRENCY, refresh)
                .subscribe { resource ->
                    tickerLiveData.postValue(resource)
                }
        }
    }
}