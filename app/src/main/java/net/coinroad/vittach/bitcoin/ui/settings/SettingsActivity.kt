package net.coinroad.vittach.bitcoin.ui.settings

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.preference.*
import android.view.MenuItem
import androidx.appcompat.app.AppCompatDelegate
import net.coinroad.vittach.R

class SettingsActivity : PreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.create(this, null).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBuildHeaders(target: List<PreferenceActivity.Header>) {
        loadHeadersFromResource(R.xml.preference_headers, target)
    }

    override fun isValidFragment(fragmentName: String) = true

    class GeneralPrefFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.preference_general)
            setHasOptionsMenu(true)

            bindPreferenceSummaryToValue(findPreference("example_text"))
            bindPreferenceSummaryToValue(findPreference("example_list"))
        }

        override fun onOptionsItemSelected(item: MenuItem) = backToHomeActivity(item, activity)
    }

    class NotificPrefFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.preference_notification)
            setHasOptionsMenu(true)

            bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"))
        }

        override fun onOptionsItemSelected(item: MenuItem) = backToHomeActivity(item, activity)
    }

    companion object {

        fun backToHomeActivity(menuItem: MenuItem, activity: Activity): Boolean {
            val id = menuItem.itemId
            if (id == android.R.id.home) {
                val intent = Intent(activity, SettingsActivity::class.java)
                activity.startActivity(intent)
                return true
            }
            return activity.onOptionsItemSelected(menuItem)
        }

        private val valueListener =
            Preference.OnPreferenceChangeListener { preference, value ->
                val stringValue = value.toString()

                if (preference is ListPreference) {
                    val index = preference.findIndexOfValue(stringValue)
                    preference.setSummary(if (index >= 0) preference.entries[index] else null)
                } else {
                    preference.summary = stringValue
                }

                true
            }

        private fun bindPreferenceSummaryToValue(preference: Preference) {
            preference.onPreferenceChangeListener = valueListener

            valueListener.onPreferenceChange(
                preference,
                PreferenceManager.getDefaultSharedPreferences(preference.context)
                    .getString(preference.key, "")
            )
        }
    }
}
