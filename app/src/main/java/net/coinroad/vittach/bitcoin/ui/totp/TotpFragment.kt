package net.coinroad.vittach.bitcoin.ui.totp

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import de.taimos.totp.TOTP
import kotlinx.android.synthetic.main.fragment_totp.*
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.BaseActivity
import net.coinroad.vittach.bitcoin.domain.models.PaymentData
import net.coinroad.vittach.bitcoin.ui.webview.WebviewFragment
import net.coinroad.vittach.bitcoin.viewmodel.TotpViewModel
import org.apache.commons.codec.binary.Base32
import org.apache.commons.codec.binary.Hex
import org.koin.android.viewmodel.ext.android.viewModel


class TotpFragment : Fragment() {

    companion object {
        var WHITE = -0x1
        var BLACK = -0x1000000
        const val WIDTH = 500
    }

    private val totpVM: TotpViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, group: ViewGroup?, bundle: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_totp, group, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // https://github.com/jchambers/java-otp

        val paymentData = PaymentData(fromAmount = "20000", fromAccount = "79117273996", email = "zz@gmail.com")
        val bundle = WebviewFragment.bundle(paymentData)

        web_button.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.nextAction, bundle)
        }
    }

    override fun onStart() {
        super.onStart()

        BaseActivity.primaryFragmentId = R.id.totp_graph

        val totpSecretKey = totpVM.getSecretKey()

        totp_key_code.setText(totpSecretKey, TextView.BufferType.EDITABLE)
        current_code.text = getTOTPCode(totpSecretKey)
        totp_qr_code.setImageBitmap(createQRCode(totpSecretKey))
    }

    private fun createQRCode(str: String): Bitmap? {
        val result = try {
            MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, WIDTH, WIDTH, null)
        } catch (iae: IllegalArgumentException) {
            return null
        }

        val w = result.width
        val h = result.height
        val pixels = IntArray(w * h)

        for (y in 0 until h) {
            val offset = y * w
            for (x in 0 until w) {
                pixels[offset + x] = if (result[x, y]) {
                    BLACK
                } else WHITE
            }
        }

        return Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888).apply {
            setPixels(pixels, 0, WIDTH, 0, 0, w, h)
        }
    }

    private fun getTOTPCode(secretKey: String): String {
        val bytes = Base32().decode(secretKey)
        val hexKey = Hex.encodeHexString(bytes)
        return TOTP.getOTP(hexKey)
    }
}
