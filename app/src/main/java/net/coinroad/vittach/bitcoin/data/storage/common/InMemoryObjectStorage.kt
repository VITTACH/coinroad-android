package net.coinroad.vittach.bitcoin.data.storage.common

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable

interface ObjectStorage<T> {
    fun observable(): Observable<T>
    fun update(newObj: T)
    fun hasValue(): Boolean
    fun valueOrNull(): T?
    fun value(): T
}

abstract class InMemoryObjectStorage<T>(
    defaultValue: T? = null
) : ObjectStorage<T> {

    private val behaviorRelay = defaultValue?.let {
        BehaviorRelay.createDefault(defaultValue)
    } ?: BehaviorRelay.create<T>()

    private val threadSafeRelay = behaviorRelay.toSerialized()

    override fun observable(): Observable<T> {
        return threadSafeRelay.hide()
    }

    override fun update(newObj: T) {
        threadSafeRelay.accept(newObj)
    }

    override fun hasValue() = behaviorRelay.hasValue()
    override fun valueOrNull() = behaviorRelay.value
    override fun value() = behaviorRelay.value!!
}