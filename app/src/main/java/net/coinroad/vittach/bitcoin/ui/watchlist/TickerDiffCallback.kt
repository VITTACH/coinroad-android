package net.coinroad.vittach.bitcoin.ui.watchlist

import androidx.recyclerview.widget.DiffUtil
import net.coinroad.vittach.bitcoin.domain.models.TickerModel

class TickerDiffCallback(
    private val newList: List<TickerModel>, private val oldList: List<TickerModel>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val res = oldList[oldPosition].lastUpdated == newList[newPosition].lastUpdated
        return res
    }
}