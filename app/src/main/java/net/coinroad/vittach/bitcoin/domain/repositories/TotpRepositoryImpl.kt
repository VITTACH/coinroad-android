package net.coinroad.vittach.bitcoin.domain.repositories

import net.coinroad.vittach.bitcoin.data.storage.TotpPreferences
import org.apache.commons.codec.binary.Base32
import java.security.SecureRandom

interface TotpRepository {
    fun getSecretKey(): String
}

class TotpRepositoryImpl(
    private val totpPreferences: TotpPreferences
) : TotpRepository {

    override fun getSecretKey(): String {
        return totpPreferences.secretKey ?: generateSecretKey()
    }

    private fun generateSecretKey(): String {
        val random = SecureRandom()
        val bytes = ByteArray(20)
        random.nextBytes(bytes)
        val secretKey = Base32().encodeToString(bytes)
        totpPreferences.secretKey = secretKey
        return secretKey
    }
}