package net.coinroad.vittach.bitcoin.viewmodel

import net.coinroad.vittach.bitcoin.domain.repositories.TotpRepository


class TotpViewModel(
    private val repository: TotpRepository
) : BaseViewModel() {

    fun getSecretKey(): String {
        return repository.getSecretKey()
    }

}