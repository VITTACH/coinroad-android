package net.coinroad.vittach.bitcoin.ui.watchlist

import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.View
import android.view.View.MeasureSpec
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import net.coinroad.vittach.bitcoin.domain.models.TickerModel
import net.coinroad.vittach.databinding.ItemAdsViewBinding
import net.coinroad.vittach.databinding.ItemTickerViewBinding

class RecyclerWatchlistAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_ADS = 0
        const val VIEW_TYPE_TICKER = 1

        const val ITEM_ROWS = 4
    }

    private var orientation = Configuration.ORIENTATION_PORTRAIT
    private var tickerModelList = mutableListOf<TickerModel>()
    private lateinit var clickListener: ItemClickListener

    private var itemViewHeight = 0
    private var adsViewHeight = 0

    private var rowSize = 0

    // parent activity will implement this method respond click events
    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): RecyclerView.ViewHolder {
        return when (type) {
            VIEW_TYPE_ADS -> {
                val view = ItemAdsViewBinding.inflate(LayoutInflater.from(parent.context))
                AdsViewHolder(view.root)
            }
            else -> {
                val view = ItemTickerViewBinding.inflate(LayoutInflater.from(parent.context))
                TickerViewHolder(view.root)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val isLandScape =
            orientation == Configuration.ORIENTATION_LANDSCAPE && isAdsRequired(position - 1)
        return if (isAdsRequired(position) || isLandScape) {
            VIEW_TYPE_ADS
        } else VIEW_TYPE_TICKER
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_ADS) {
            return
        } else {
            (holder as TickerViewHolder).binding?.ticker = tickerModelList[position]
        }
    }

    // Return the size of tickerModelList (invoked by layout manager)
    override fun getItemCount(): Int {
        return tickerModelList.size
    }

    private fun isAdsRequired(position: Int): Boolean {
        return position >= 0 && position % (rowSize * ITEM_ROWS + orientation) == 0
    }

    fun removeItems(startIndex: Int = 0, endIndex: Int) {
        tickerModelList.subList(startIndex, endIndex).clear()
        notifyItemRangeRemoved(startIndex, endIndex - startIndex)
    }

    fun addTicker(model: TickerModel, position: Int) {
        tickerModelList.add(position, model)
        notifyItemInserted(position)
    }

    fun getRowHeight(type: Int) = if (type == VIEW_TYPE_ADS) adsViewHeight else itemViewHeight

    // convenience method for the getting data at click position
    fun getItem(index: Int): TickerModel = tickerModelList[index]

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener) {
        this.clickListener = itemClickListener
    }

    fun setLayoutOrientation(orientation: Int) {
        this.orientation = orientation
    }

    fun setRowSize(rowSize: Int) {
        this.rowSize = rowSize
    }

    fun setTickers(tickers: ArrayList<TickerModel>) {
        val diffResult = DiffUtil.calculateDiff(TickerDiffCallback(tickers, tickerModelList))

        tickerModelList.clear()
        tickerModelList.addAll(tickers)

        diffResult.dispatchUpdatesTo(this)
    }

    // stores and recycles views as they are scrolled off screen
    inner class TickerViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        var binding: ItemTickerViewBinding? = DataBindingUtil.bind(view)

        init {
            view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED)
            itemViewHeight = view.measuredHeight

            view.setOnClickListener(this)
        }

        override fun onClick(bindView: View) {
            clickListener.onItemClick(bindView, adapterPosition)
        }
    }

    inner class AdsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED)
            adsViewHeight = view.measuredHeight
        }
    }
}