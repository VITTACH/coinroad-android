package net.coinroad.vittach.bitcoin.ui.themes

import net.coinroad.vittach.bitcoin.BaseActivity
import android.animation.Animator
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.ImageView
import androidx.drawerlayout.widget.DrawerLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_screenshot.*
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.util.CubicBezierInterpolator
import kotlin.math.max

class ScreenshotActivity : BaseActivity() {

    companion object {
        const val CENTER_X = "CENTER_X"
        const val CENTER_Y = "CENTER_Y"
        const val BITMAP = "BITMAP"

        const val REQUEST_CHANGE_THEME = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screenshot)

        val byteArray = intent.getByteArrayExtra(BITMAP)
        val screenshot = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
        val centerX = intent.getIntExtra(CENTER_X, -1)
        val centerY = intent.getIntExtra(CENTER_Y, -1)

        contentView.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewDetachedFromWindow(v: View?) {}

            override fun onViewAttachedToWindow(v: View?) {
                startCircularAnimation(screenshot, centerX, centerY)
            }
        })

        drawer_layout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN)

        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        }
    }

    private fun startCircularAnimation(bitmap: Bitmap, centerX: Int, centerY: Int) {
        contentView.setImageBitmap(bitmap)
        contentView.scaleType = ImageView.ScaleType.MATRIX
        contentView.visibility = View.VISIBLE

        // Final radius is approximated here
        val endRadius = max(bitmap.height, bitmap.width).toFloat()
        ViewAnimationUtils.createCircularReveal(contentView, centerX, centerY, endRadius, 0f)
            .apply {
                duration = 500
                interpolator = CubicBezierInterpolator.EASE_IN_OUT_QUAD
                addListener(object : Animator.AnimatorListener {
                    override fun onAnimationEnd(animation: Animator?) {
                        contentView.setImageDrawable(null)
                        contentView.visibility = View.GONE
                        setResult(REQUEST_CHANGE_THEME, Intent())
                        finish()
                    }

                    override fun onAnimationCancel(animation: Animator?) {}
                    override fun onAnimationRepeat(animation: Animator?) {}
                    override fun onAnimationStart(animation: Animator?) {}
                })
            }
            .start()
    }
}