package net.coinroad.vittach.bitcoin.util

import android.content.Context


fun Float.dpFromPx(context: Context): Float {
    return this / context.resources.displayMetrics.density
}

fun Float.pxFromDp(context: Context): Float {
    return this * context.resources.displayMetrics.density
}
