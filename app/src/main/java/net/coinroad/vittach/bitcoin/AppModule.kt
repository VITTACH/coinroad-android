package net.coinroad.vittach.bitcoin

import net.coinroad.vittach.bitcoin.data.network.ApiFactory
import net.coinroad.vittach.bitcoin.data.network.cmc.CMCApiService
import net.coinroad.vittach.bitcoin.data.storage.*
import net.coinroad.vittach.bitcoin.viewmodel.TickerViewModel
import net.coinroad.vittach.bitcoin.viewmodel.TotpViewModel
import net.coinroad.vittach.bitcoin.domain.repositories.*
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

object AppModule {

    fun create() = module {
        single<TotpPreferences> { TotpPreferencesImpl(androidContext()) }
        single<ThemePreferences> { ThemePreferencesImpl(androidContext()) }
        single<WidgetPreferences> { WidgetPreferencesImpl(androidContext()) }

        single<TickerRepository> { TickerRepositoryImpl(get()) }
        single<TotpRepository> { TotpRepositoryImpl(get()) }
        single<ThemeRepository> { ThemeRepositoryImpl(get()) }
    }
}

object NetworkModule {

    fun create() = module {
        single { CMCApiService(get()) }
        single { ApiFactory() }
    }
}

object ViewmodelModule {

    fun create() = module {
        viewModel { TickerViewModel(get()) }
        viewModel { TotpViewModel(get()) }
    }
}
