package net.coinroad.vittach.bitcoin.data.storage.common

import android.content.Context
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.io.File

abstract class PersistentObjectStorage<T>(
    private val reference: TypeReference<T>,
    context: Context,
    storageKey: String,
    defaultValue: T?
) : InMemoryObjectStorage<T>() {

    private val objectMapper = ObjectMapper()

    private val storageFile = File(context.filesDir, storageKey)

    init {
        Completable
            .fromAction {
                try {
                    (readFromFile() ?: defaultValue)?.let { update(it) }
                } catch (e: Throwable) {
                    Timber.e(e)
                    defaultValue?.let { update(it) }
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorComplete()
            .subscribe()

        observable()
            .switchMapCompletable {
                Completable
                    .fromAction { writeToFile(it) }
                    .doOnError { Timber.e(it) }
                    .onErrorComplete()
            }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    private fun writeToFile(newObj: T) {
        if (!storageFile.exists()) {
            storageFile.createNewFile()
        }

        storageFile.writeText(objectMapper.writeValueAsString(newObj))
    }

    private fun readFromFile(): T? {
        return if (storageFile.exists()) {
            objectMapper.readValue(storageFile.readText(), reference)
        } else {
            null
        }
    }
}