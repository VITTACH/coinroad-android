package net.coinroad.vittach.bitcoin.data.storage

import android.content.Context
import com.chibatching.kotpref.KotprefModel
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException

interface WidgetPreferences {
    var instruments: String
    var interval: Int

    fun readPrefs(): HashMap<Int, String>?
    fun writePrefs(instrumentsMap: HashMap<Int, String>)
}

class WidgetPreferencesImpl(context: Context) : WidgetPreferences, KotprefModel(context) {
    override var instruments: String by stringPref()
    override var interval: Int by intPref()

    private val type = object : TypeReference<HashMap<Int, String>>() {}

    override fun readPrefs(): HashMap<Int, String>? {
        try {
            return ObjectMapper().readValue(instruments, type)
        } catch (e: Exception) {}
        return null
    }

    override fun writePrefs(instrumentsMap: HashMap<Int, String>) {
        try {
            instruments = ObjectMapper().writeValueAsString(instrumentsMap)
        } catch (ioe: IOException) {}
    }
}