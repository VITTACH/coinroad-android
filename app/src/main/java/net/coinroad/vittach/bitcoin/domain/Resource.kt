package net.coinroad.vittach.bitcoin.domain

sealed class Resource<out T>(open val data: T? = null)

object None : Resource<Nothing>() {
    override fun toString() = "None"
}

data class Loading<out T>(override val data: T? = null) : Resource<T>(data)

data class Success<out T>(override val data: T) : Resource<T>(data)

data class Failure<out T>(
    val throwable: Throwable,
    override val data: T? = null
) : Resource<T>(data)
