package net.coinroad.vittach.bitcoin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.activity_main.*
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.domain.repositories.ThemeRepository
import net.coinroad.vittach.bitcoin.util.setupWithNavController
import org.koin.android.ext.android.inject

open class BaseActivity : AppCompatActivity() {

    protected var isDarkMode: Boolean = false

    protected val themeRepository: ThemeRepository by inject()

    private var navController: LiveData<NavController>? = null

    private val navGraphIds =
        listOf(
            R.navigation.watchlist_graph,
            R.navigation.news_graph,
            R.navigation.totp_graph
        )

    companion object {
        var primaryFragmentId: Int = R.id.watchlist_graph
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isDarkMode = themeRepository.getTheme()
        setTheme(if (isDarkMode) R.style.CustomDark else R.style.CustomLight)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController?.value?.navigateUp() ?: false
    }

    open fun updateRowIndex(index: Int) {}

    protected fun setupBottomNavigationBar() {
        navController = bottomNav.setupWithNavController(
            primaryFragmentId = primaryFragmentId,
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.nav_host_container,
            intent = intent
        )
    }

    fun getToolBarHeight(): Int {
        return toolbar.height
    }

    fun setAppBarVisibility(visible: Boolean) {
        val hostContainer = nav_host_container ?: return
        with(hostContainer.layoutParams as CoordinatorLayout.LayoutParams) {
            appbar.isVisible = visible
            behavior = if (visible) {
                AppBarLayout.ScrollingViewBehavior()
            } else {
                null
            }
        }
        hostContainer.requestLayout()
    }
}