package net.coinroad.vittach.bitcoin.domain.repositories

import net.coinroad.vittach.bitcoin.data.storage.ThemePreferences

interface ThemeRepository {
    fun getTheme(): Boolean
    fun setTheme(isDarkTheme: Boolean)
}

class ThemeRepositoryImpl(
    private val themePreferences: ThemePreferences
) : ThemeRepository {

    override fun getTheme(): Boolean {
        return themePreferences.theme
    }

    override fun setTheme(isDarkTheme: Boolean) {
        themePreferences.theme = isDarkTheme
    }
}