package net.coinroad.vittach.bitcoin.widget

import android.app.job.JobInfo
import android.app.PendingIntent
import android.app.job.JobService
import android.app.job.JobScheduler
import android.app.job.JobParameters
import android.content.ComponentName
import android.appwidget.AppWidgetManager
import io.reactivex.schedulers.Schedulers
import com.firebase.jobdispatcher.Trigger
import androidx.annotation.RequiresApi
import android.os.Build
import android.content.Context
import io.reactivex.Observable
import android.view.View
import android.widget.RemoteViews
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import net.coinroad.vittach.bitcoin.data.network.ApiFactory
import com.firebase.jobdispatcher.Lifetime
import io.reactivex.disposables.Disposable
import android.appwidget.AppWidgetProvider
import com.github.ajalt.timberkt.d
import com.github.ajalt.timberkt.e
import io.reactivex.Single
import android.content.Intent
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.data.entity.TickerData
import net.coinroad.vittach.bitcoin.data.network.cmc.CMCApiService
import net.coinroad.vittach.bitcoin.data.storage.WidgetPreferences
import org.koin.java.standalone.KoinJavaComponent.get
import java.util.*

data class WidgetModel(val ticker: TickerData?, val tickerId: String)

abstract class WidgetProvider : AppWidgetProvider() {

    override fun onDeleted(context: Context, appWidgetIds: IntArray) {
        val prefs = get(WidgetPreferences::class.java)
        prefs.readPrefs()?.let { inst ->
            appWidgetIds.forEach { widgetId -> inst.remove(widgetId) }
            if (inst.size == 0) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    FirebaseJobDispatcher(GooglePlayDriver(context)).cancel(UPDATE_JOB_TAG)
                } else context.getSystemService(JobScheduler::class.java).cancelAll()
            }
            prefs.writePrefs(inst)
        }
    }

    override fun onUpdate(context: Context?, appManager: AppWidgetManager?, widgetIds: IntArray?) {
        super.onUpdate(context, appManager, widgetIds)
        startUpdateJob(context, widgetIds)
    }

    enum class WidgetType {
        Large,
        Medium,
        Small
    }

    companion object {
        const val WIDGET_CURRENCY = "USD"

        private const val updateInterval = 5
        private const val UPDATE_JOB_TAG = "coinroad_widget_update"

        fun startUpdateJob(context: Context?, widgetIds: IntArray?) {
            if (widgetIds == null || context == null) return

            Single.zip(
                arrayListOf(
                    updateWidgets(context, WidgetType.Large),
                    updateWidgets(context, WidgetType.Medium),
                    updateWidgets(context, WidgetType.Small)
                )
            ) { }.subscribeOn(Schedulers.io()).subscribe()

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                scheduleFirebaseUpdJob(context)
            } else {
                scheduleUpdJob(context)
            }
        }

        @RequiresApi(Build.VERSION_CODES.M)
        fun scheduleUpdJob(context: Context) {
            val interval = get(WidgetPreferences::class.java).interval.let {
                return@let if (it == 0) updateInterval * 60 else it
            }

            val builder = JobInfo.Builder(0, ComponentName(context, WidgetJobService::class.java));
            builder.setMinimumLatency(interval * 1000L)
            builder.setOverrideDeadline(interval * 1000L)

            val job = context.getSystemService(JobScheduler::class.java)?.schedule(builder.build())
            if (job == JobScheduler.RESULT_SUCCESS) {
                d { "Job scheduled successfully" }
            }
        }

        fun scheduleFirebaseUpdJob(context: Context) {
            val interval = get(WidgetPreferences::class.java).interval.let {
                return@let if (it == 0) updateInterval * 60 else it
            }

            val executionWindowEnd = interval * 1.1
            val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(context))
            val myJob = dispatcher.newJobBuilder()
                .setService(FirebaseJobService::class.java)
                .setTag(UPDATE_JOB_TAG)
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(interval, executionWindowEnd.toInt()))
                .setLifetime(Lifetime.FOREVER)
                .setReplaceCurrent(false)
                .build()

            dispatcher.mustSchedule(myJob)
        }

        @RequiresApi(Build.VERSION_CODES.M)
        class WidgetJobService : JobService() {
            private var disposable: Disposable? = null

            override fun onStartJob(jobParams: JobParameters): Boolean {
                disposable?.dispose()
                disposable = Single.zip(
                    arrayListOf(
                        updateWidgets(applicationContext, WidgetType.Large),
                        updateWidgets(applicationContext, WidgetType.Medium),
                        updateWidgets(applicationContext, WidgetType.Small)
                    )
                ) { }
                    .subscribeOn(Schedulers.io())
                    .subscribe { res ->
                        scheduleUpdJob(applicationContext)
                    }
                return true
            }

            override fun onStopJob(jobParams: JobParameters): Boolean {
                disposable?.dispose()
                return true
            }
        }

        class FirebaseJobService : com.firebase.jobdispatcher.JobService() {
            private var disposable: Disposable? = null

            override fun onStartJob(jobParam: com.firebase.jobdispatcher.JobParameters): Boolean {
                disposable?.dispose()
                disposable = Single.zip(
                    arrayListOf(
                        updateWidgets(applicationContext, WidgetType.Large),
                        updateWidgets(applicationContext, WidgetType.Medium),
                        updateWidgets(applicationContext, WidgetType.Small)
                    )
                ) { }
                    .subscribeOn(Schedulers.io())
                    .subscribe { res ->
                        scheduleFirebaseUpdJob(applicationContext)
                    }
                return true
            }

            override fun onStopJob(jobParam: com.firebase.jobdispatcher.JobParameters?): Boolean {
                disposable?.dispose()
                return true
            }
        }

        private fun getView(ctxt: Context?, id: Int, type: WidgetType, model: WidgetModel? = null):
                RemoteViews {
            val layoutType = when (type) {
                WidgetType.Large -> R.layout.widget_large_layout
                WidgetType.Medium -> R.layout.widget_medium_layout
                WidgetType.Small -> R.layout.widget_small_layout
            }
            return RemoteViews(ctxt?.packageName, layoutType).apply {
                val tent = Intent(ctxt, WidgetActivity::class.java).apply {
                    putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id)
                }
                setOnClickPendingIntent(R.id.layout, PendingIntent.getActivity(ctxt, id, tent, 0))

                model?.let { model ->
                    val ticker = model.ticker
                    ticker?.let {
                        val precision =
                            ticker.quote?.detail?.get(WIDGET_CURRENCY)?.priceUsd?.stripTrailingZeros()?.scale()
                                ?: 8

                        when (type) {
                            WidgetType.Large ->
                                WidgetProviderLarge.bindView(
                                    ticker,
                                    this,
                                    precision,
                                    WIDGET_CURRENCY
                                )
                            WidgetType.Medium ->
                                WidgetProviderMedium.bindView(
                                    ticker,
                                    this,
                                    precision,
                                    WIDGET_CURRENCY
                                )
                            WidgetType.Small ->
                                WidgetProviderSmall.bindView(
                                    ticker,
                                    this,
                                    precision,
                                    WIDGET_CURRENCY
                                )
                        }

                        setViewVisibility(R.id.widget_progress, View.GONE)
                        return@apply
                    } ?: setTextViewText(
                        R.id.exchange, ctxt?.getString(R.string.widget_failed_connect)
                    )
                } ?: setTextViewText(R.id.exchange, ctxt?.getString(R.string.widget_default_info))

                setViewVisibility(R.id.widget_progress, View.VISIBLE)
            }
        }

        private fun readNewWidgetPref(id: Int): Pair<String?, String?> {
            return get(WidgetPreferences::class.java).readPrefs()?.get(id)?.let {
                Pair(it, it)
            } ?: Pair(null, null)
        }

        private fun buildUpdate(context: Context?, id: Int, type: WidgetType) =
            Observable.create<RemoteViews> { emitter ->
                val (exchange, pair) = readNewWidgetPref(id)
                if (exchange == null || pair == null) {
                    return@create emitter.onNext(getView(context, id, type))
                }
                getView(context, id, type)

                val coin = "bitcoin"

                get(CMCApiService::class.java)
                    .getTicker(coin)
                    .subscribe(
                        { ticker ->
                            val model = ticker.data?.get(0)
                            emitter.onNext(getView(context, id, type, WidgetModel(model, coin)))
                        },
                        { e { "error tickerModel $id" } }
                    )
            }

        private fun updateWidgets(ctxt: Context, type: WidgetType, widgetIds: IntArray? = null) =
            Single.create<Boolean> { emitter ->
                var updateWidgetIds = widgetIds
                val myManager = AppWidgetManager.getInstance(ctxt)

                if (updateWidgetIds == null) {
                    try {
                        val component = when (type) {
                            WidgetType.Large ->
                                ComponentName(ctxt, WidgetProviderLarge::class.java)
                            WidgetType.Medium ->
                                ComponentName(ctxt, WidgetProviderMedium::class.java)
                            WidgetType.Small ->
                                ComponentName(ctxt, WidgetProviderSmall::class.java)
                        }
                        updateWidgetIds = myManager.getAppWidgetIds(component)
                    } catch (e: RuntimeException) {
                        e { "Runtime Exception when request getAppWidgetId: $e" }
                        emitter.onSuccess(false)
                    }
                }

                if (updateWidgetIds?.isEmpty() == false) {
                    val observables = ArrayList<Observable<RemoteViews>>()
                    for (updateWidgetId in updateWidgetIds) {
                        if (updateWidgetId > AppWidgetManager.INVALID_APPWIDGET_ID) {
                            observables.add(
                                buildUpdate(ctxt, updateWidgetId, type).doOnNext { view ->
                                    myManager.updateAppWidget(updateWidgetId, view)
                                })
                        }
                    }

                    Observable.zip(observables) { }
                        .subscribe(
                            { emitter.onSuccess(true) }, { emitter.onSuccess(false) }
                        )
                } else {
                    e { "Can't update widget, updateWidgetIds is null" }
                    emitter.onSuccess(false)
                }
            }
    }
}