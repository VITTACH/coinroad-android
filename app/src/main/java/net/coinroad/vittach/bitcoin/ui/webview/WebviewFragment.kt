package net.coinroad.vittach.bitcoin.ui.webview

import android.annotation.TargetApi
import android.app.Instrumentation
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context.CLIPBOARD_SERVICE
import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_webviewer.*
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.domain.models.PaymentData
import net.coinroad.vittach.bitcoin.util.pxFromDp


class WebviewFragment : Fragment() {

    companion object {
        private const val WWPAY_QIWI_USDT_URL = "https://ww-pay.com/exchange/qiwirub-to-usdterc"

        private const val PAYMENT_DATA = "payment_data"

        fun bundle(data: PaymentData) = Bundle().apply { putSerializable(PAYMENT_DATA, data) }
    }

    enum class STATES {
        FROM_AMOUNT,
        FROM_ACCOUNT,
        TO_ACCOUNT,
        EMAIL,
        SUBMIT
    }

    interface ResponseEvent {
        fun callBack(newState: STATES)
    }

    private class ClientTask(
        private val re: ResponseEvent,
        private val state: STATES,
        private val instrumentation: Instrumentation
    ) : Runnable {
        override fun run() {
            try {
                instrumentation.sendKeyDownUpSync(KeyEvent.KEYCODE_PASTE)
                re.callBack(STATES.values()[state.ordinal + 1])
            } catch (e: InterruptedException) {
            }
        }
    }

    private val instrumentation = Instrumentation()

    private val handler = Handler()

    private lateinit var mActivity: FragmentActivity

    private lateinit var paymentData: PaymentData

    private lateinit var clipBoardManager: ClipboardManager

    private var state = STATES.FROM_AMOUNT

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = requireActivity()
        clipBoardManager = mActivity.getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        paymentData = (arguments?.getSerializable(PAYMENT_DATA) as? PaymentData)
            ?: throw IllegalArgumentException("Payment data required")
    }

    override fun onCreateView(inflater: LayoutInflater, group: ViewGroup?, bundle: Bundle?): View? {
        mActivity.onBackPressedDispatcher.addCallback(this) {
            onBackPressed()
        }

        return inflater.inflate(R.layout.fragment_webviewer, group, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(webView) {
            webViewClient = MyWebViewClient()
            settings.javaScriptEnabled = true
            loadUrl(WWPAY_QIWI_USDT_URL)
            // setOnTouchListener { v, event -> event.action == MotionEvent.ACTION_MOVE }
        }
    }

    private inner class MyWebViewClient : WebViewClient() {
        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
            view.loadUrl(request.url.toString())
            return true
        }

        // Для старых устройств
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)

            runJsScript("document.getElementById(\"customControlInline\").click();") {
                handleState(state)
            }
        }
    }

    private fun handleState(state: STATES) {

        when (state) {
            STATES.FROM_AMOUNT -> {
                runJsScript("document.getElementById(\"fromAmount\").scrollIntoView();") {
                    setClipboardText(paymentData.fromAmount)
                }
            }
            STATES.FROM_ACCOUNT -> {
                runJsScript("document.getElementById(\"fromAccount\").scrollIntoView();") {
                    setClipboardText(paymentData.fromAccount)
                }
            }
            STATES.TO_ACCOUNT -> {
                runJsScript("document.getElementById(\"toAccount\").scrollIntoView();") {
                    setClipboardText(paymentData.toAccount)
                }
            }
            STATES.EMAIL -> {
                runJsScript("document.getElementById(\"email\").scrollIntoView();") {
                    setClipboardText(paymentData.email)
                }
            }
        }
    }

    private fun runJsScript(script: String, callBack: () -> Unit) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript(script) {
                callBack()
            }
        } else webView.loadUrl("javascript:$script")
    }

    private fun setClipboardText(value: String) {
        clipBoardManager.setPrimaryClip(ClipData.newPlainText("", value))

        onClickEvent(MotionEvent.ACTION_DOWN, MotionEvent.ACTION_UP)

        val client = ClientTask(object : ResponseEvent {
            override fun callBack(newState: STATES) {
                state = newState

                handler.postDelayed({
                    if (state == STATES.SUBMIT) {
                        hideKeyboard()
                    } else {
                        handleState(state)
                    }
                }, 500)
            }
        }, state, instrumentation)

        Thread(client).start()
    }

    private fun hideKeyboard() {
        val imm = mActivity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mActivity.currentFocus!!.windowToken, 0)
    }

    private fun onClickEvent(vararg events: Int) {
        val initX = 100f.pxFromDp(requireContext())
        val initY = (when (state) {
            STATES.EMAIL -> 50f
            STATES.SUBMIT -> 100f
            else -> 0f
        }).pxFromDp(requireContext())

        val downTime = SystemClock.uptimeMillis()

        events.forEach { event ->
            val x = if (event == MotionEvent.ACTION_MOVE) {
                initX - 30
            } else {
                initX
            }
            val me = MotionEvent.obtain(downTime, SystemClock.uptimeMillis(), event, x, initY, 0)

            webView.dispatchTouchEvent(me)
        }
    }

    private fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            findNavController().popBackStack()
        }
    }
}
