package net.coinroad.vittach.bitcoin.data.storage

import android.content.Context
import com.chibatching.kotpref.KotprefModel
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException

interface ThemePreferences {
    var theme: Boolean
}

class ThemePreferencesImpl(context: Context) : ThemePreferences, KotprefModel(context) {
    override var theme: Boolean by booleanPref(false)
}