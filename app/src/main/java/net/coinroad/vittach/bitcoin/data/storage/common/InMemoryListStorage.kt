package net.coinroad.vittach.bitcoin.data.storage.common

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable

typealias Id = Long

interface Identifiable {
    val id: Id
}

interface CollectionStorage<T : Identifiable> {
    fun list(): List<T>
    fun listObservable(): Observable<List<T>>
    fun itemObservable(id: Id): Observable<T>
    fun replaceAll(items: List<T>)
    fun addAll(items: List<T>)
    fun updateAll(items: List<T>)
    fun clear()
}

class Wrapper<T>(val item: T?)

abstract class InMemoryListStorage<T : Identifiable> : CollectionStorage<T> {

    private val behaviorRelay = BehaviorRelay.createDefault<List<T>>(emptyList())
    private val threadSafeRelay = behaviorRelay.toSerialized()

    override fun list(): List<T> {
        return behaviorRelay.value ?: emptyList()
    }

    override fun listObservable(): Observable<List<T>> {
        return threadSafeRelay.hide()
    }

    override fun itemObservable(id: Id): Observable<T> {
        return listObservable()
            .map { list ->
                Wrapper(list.find { it.id == id })
            }
            .filter { it.item != null }
            .map { it.item!! }
    }

    override fun addAll(items: List<T>) {
        threadSafeRelay.accept(
            threadSafeRelay.blockingFirst().toMutableList().apply { addAll(items) }
        )
    }

    override fun updateAll(items: List<T>) {
        threadSafeRelay.accept(
            threadSafeRelay.blockingFirst()
                .toMutableList()
                .map { item ->
                    items.find { item.id == it.id } ?: item
                }
        )
    }

    override fun clear() {
        threadSafeRelay.accept(emptyList())
    }

    override fun replaceAll(items: List<T>) {
        threadSafeRelay.accept(items)
    }
}