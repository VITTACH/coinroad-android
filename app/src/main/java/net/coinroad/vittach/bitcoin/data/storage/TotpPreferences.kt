package net.coinroad.vittach.bitcoin.data.storage

import android.content.Context
import com.chibatching.kotpref.KotprefModel
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException

interface TotpPreferences {
    var secretKey: String?
}

class TotpPreferencesImpl(context: Context) : TotpPreferences, KotprefModel(context) {
    override var secretKey: String? by nullableStringPref()
}