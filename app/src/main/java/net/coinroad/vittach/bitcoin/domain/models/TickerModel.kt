package net.coinroad.vittach.bitcoin.domain.models

import java.io.Serializable
import java.math.BigDecimal

interface AbstractModel {
    fun populateString()
}

class TickerModel(
    val id: String,
    val name: String,
    val symbol: String,
    val lastUpdated: String,
    val percentChange7d: String,
    val percentChange1h: String,
    val percentChange24h: String,
    val marketCapUsd: BigDecimal,
    val volumeUsd24h: BigDecimal,
    val priceUsd: BigDecimal
) : Serializable, AbstractModel {

    override fun populateString() {

    }
}