package net.coinroad.vittach.bitcoin.util

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

object DecimalUtils {
    private var priceFormat: DecimalFormat? = null
    private val PRICE_FORMAT_LOCK = Any()

    private fun initPriceFormat() {
        priceFormat = DecimalFormat().apply {
            roundingMode = RoundingMode.HALF_DOWN
            decimalFormatSymbols = DecimalFormatSymbols()
        }
    }

    fun formatNumber(value: BigDecimal, min: Int, max: Int, group: Boolean): String {
        synchronized(PRICE_FORMAT_LOCK) {
            if (priceFormat == null) {
                initPriceFormat()
            }
            priceFormat?.apply {
                groupingSize = if (group) 3 else 0
                minimumFractionDigits = min
                maximumFractionDigits = max
            }
            return priceFormat!!.format(value)
        }
    }

    fun formatPrice(value: Double?, pxPrecision: Int): String? {
        return if (value != null ) {
            formatNumber(BigDecimal.valueOf(value), pxPrecision, pxPrecision, true)
        } else null
    }

    fun formatPrice(value: BigDecimal?, pricePrecision: Int): String? {
        return if (value != null ) {
            formatNumber(value, pricePrecision, pricePrecision, true)
        } else null
    }
}