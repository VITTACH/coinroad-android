package net.coinroad.vittach.bitcoin.data.network

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import net.coinroad.vittach.bitcoin.data.network.cmc.CMCApiService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ApiFactory {
    
    val okHttpClient by lazy { createOkHttpClient() }

    fun createObjectMapper(): ObjectMapper {
        return ObjectMapper().apply {
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            configure(MapperFeature.USE_GETTERS_AS_SETTERS, false)
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        }
    }

    private fun createOkHttpClient(
        connectTimeoutInSeconds: Long = 30,
        readTimeoutInSeconds: Long = 30,
        writeTimeoutInSeconds: Long = 30
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()

        with(builder) {
            readTimeout(readTimeoutInSeconds, TimeUnit.SECONDS)
            writeTimeout(writeTimeoutInSeconds, TimeUnit.SECONDS)
            connectTimeout(connectTimeoutInSeconds, TimeUnit.SECONDS)

            addInterceptor(loggingInterceptor())
            addInterceptor(curlLoggingInterceptor())
        }

        return builder.build()
    }

    private fun loggingInterceptor(): Interceptor {
        return HttpLoggingInterceptor { message ->
            Timber.tag("OkHttp").d(message)
        }.apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    private fun curlLoggingInterceptor(): Interceptor {
        return CurlLoggingInterceptor(
            HttpLoggingInterceptor.Logger { message ->
                Timber.tag("Curl").d(message)
            })
    }
}