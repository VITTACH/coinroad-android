package net.coinroad.vittach.bitcoin.data.network.cmc

import io.reactivex.Single
import net.coinroad.vittach.bitcoin.data.entity.BaseResponse
import net.coinroad.vittach.bitcoin.data.entity.TickerData
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface CMCApi {
    @GET("/v1/cryptocurrency/listings/latest")
    fun getTickers(
        @Query("convert") currency: String,
        @Header("X-CMC_PRO_API_KEY") apiKey: String
    ): Single<BaseResponse<TickerData>>

    @GET("v1/cryptocurrency/quotes/latest")
    fun getTicker(
        @Query("id") tickerId: String,
        @Header("X-CMC_PRO_API_KEY") apiKey: String
    ): Single<BaseResponse<TickerData>>
}