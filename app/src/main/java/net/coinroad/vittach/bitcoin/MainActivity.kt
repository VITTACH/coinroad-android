package net.coinroad.vittach.bitcoin

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import com.jraska.falcon.Falcon
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawer_head_view.*
import net.coinroad.vittach.R
import net.coinroad.vittach.bitcoin.ui.themes.ScreenshotActivity
import net.coinroad.vittach.bitcoin.ui.themes.ScreenshotActivity.Companion.BITMAP
import net.coinroad.vittach.bitcoin.ui.themes.ScreenshotActivity.Companion.REQUEST_CHANGE_THEME
import net.coinroad.vittach.bitcoin.ui.themes.ScreenshotActivity.Companion.CENTER_X
import net.coinroad.vittach.bitcoin.ui.themes.ScreenshotActivity.Companion.CENTER_Y
import java.io.ByteArrayOutputStream


class MainActivity : BaseActivity() {
    // medium.com/@guendouz/room-livedata-and-recyclerview-d8e96fb31dfe
    // https://developer.android.com/jetpack/docs/guide

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        setupBottomNavigationBar()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null) return
        if (requestCode == REQUEST_CHANGE_THEME) {
            recreate()
        }
    }

    override fun updateRowIndex(index: Int) {
        rowIndexView.text = index.toString()
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val height = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (height > 0) {
            result = resources.getDimensionPixelSize(height)
        }
        return result
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        nightModeBtn.setOnClickListener {
            newThemeSelected()
        }

        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        }
    }

    private fun newThemeSelected() {
        themeRepository.setTheme(!isDarkMode)

        val windowBitmap = Falcon.takeScreenshotBitmap(this)
        val statusBarHeight = getStatusBarHeight()
        val bitmap = Bitmap.createBitmap(
            windowBitmap,
            0,
            statusBarHeight,
            windowBitmap.width,
            windowBitmap.height - statusBarHeight,
            null,
            true
        )

        val startPointX = nightModeBtn.x.toInt() + nightModeBtn.width / 2
        val startPointY = nightModeBtn.y.toInt() + nightModeBtn.height / 2

        val byteStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteStream)
        val byteArray = byteStream.toByteArray()

        val intent = Intent(this, ScreenshotActivity::class.java).apply {
            putExtra(BITMAP, byteArray)
            putExtra(CENTER_X, startPointX)
            putExtra(CENTER_Y, startPointY)
            addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        }

        startActivityForResult(intent, REQUEST_CHANGE_THEME)
        overridePendingTransition(0, 0)
    }
}
