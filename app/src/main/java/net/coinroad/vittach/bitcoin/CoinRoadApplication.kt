package net.coinroad.vittach.bitcoin

import android.app.Application
import com.github.ajalt.timberkt.Timber
import net.coinroad.vittach.BuildConfig
import net.coinroad.vittach.bitcoin.di.AppComponent
import net.coinroad.vittach.bitcoin.di.DaggerAppComponent
import net.coinroad.vittach.bitcoin.di.ProviderModule
import org.koin.android.ext.android.startKoin

class CoinRoadApplication : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        initLogger()
        initializeKoin()
        // initializeDagger()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initializeDagger() {
        appComponent = DaggerAppComponent
            .builder()
            .providerModule(ProviderModule())
            .build()
    }

    private fun initializeKoin() {
        startKoin(
            this,
            listOf(
                AppModule.create(),
                NetworkModule.create(),
                ViewmodelModule.create()
            )
        )
    }
}